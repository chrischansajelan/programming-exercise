<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Methods, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

include_once '../../config/database.php';
include_once '../../models/webinar.php';

$database = new Database();
$db = $database->connect();

$webinar = new Webinar($db);

$data = json_decode(file_get_contents("php://input"));

$webinar->fullname = $data->fullname;
$webinar->email = $data->email;

// echo json_encode(
//   array(
//     'fullname' => $webinar->fullname,
//     'email' => $webinar->email
//   )
// );

if($webinar->addUser()) {
  echo json_encode(
    array('message' => 'User has been added')
  );
} else {
  echo json_encode(
    array('message' => 'Failed to add the user')
  );
}

?>