<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/database.php';
include_once '../../models/webinar.php';

$database = new Database();
$db = $database->connect();

$webinar = new Webinar($db);

$resut = $webinar->getAllWebinars();
$num = $resut->rowCount();

if($num > 0) {
  $webinar_arr = array();
  $webinar_arr['data'] = array();

  while($row = $resut->fetch(PDO::FETCH_ASSOC)) {
    extract($row);

    $webinar_item = array(
      'id' => $id,
      'webinar_id' => $webinar_id,
      'name' => $name,
      'scheduled_date' => $scheduled_date,
      'header_image' => base64_encode($header_image),
    );

    array_push($webinar_arr['data'], $webinar_item);
  }

  echo json_encode($webinar_arr);
} else {
  echo json_encode(
    array('message' => 'No Data Found')
  );
}
?>