<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/database.php';
include_once '../../models/webinar.php';

$database = new Database();
$db = $database->connect();

$webinar = new Webinar($db);
$webinar->webinar_id = isset($_GET['webinarid']) ? $_GET['webinarid'] : die();


$webinar->getSingleWebinar();

$webinar_arr = array(
  'webinar_id' => $webinar->webinar_id,
  'name' => $webinar->name,
  'brief_desc' => $webinar->brief_desc,
  'scheduled_date' => $webinar->scheduled_date,
  'header_image' => base64_encode($webinar->header_image)
);


print_r(json_encode($webinar_arr));