<?php
$dbh = new PDO('mysql:host=localhost;dbname=programming_exercise', 'root', '');
if(isset($_POST['btn'])){
  $name = $_FILES['foto']['name'];
  $type = $_FILES['foto']['type'];
  $data = file_get_contents($_FILES['foto']['tmp_name']);
  $stmt = $dbh->prepare("insert into tbl_webinars(header_image) values(?)");
  $stmt->bindParam(1, $data);
  $stmt->execute();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Page 1</title>
</head>
<body>


  <form method="POST" enctype="multipart/form-data">
    <input type="file" name="foto">
    <button name=btn>Upload</button>
  </form>
</body>
</html>