<?php
class Webinar {
  private $conn;
  private $table = 'webinars';

  public $id;
  public $webinar_id;
  public $name;
  public $brief_desc;
  public $scheduled_date;
  public $header_image;
  
  public $fullname;
  public $email;

  public function __construct($db) {
    $this->conn = $db;
  }

  public function getAllWebinars() {
    $query = 'SELECT * FROM tbl_webinars';

    $stmt = $this->conn->prepare($query);

    $stmt->execute();

    return $stmt;
  }

  public function getSingleWebinar() {
    $query = "SELECT * FROM tbl_webinars where webinar_id = '".$this->webinar_id."'";

    $stmt = $this->conn->prepare($query);
    $stmt->execute();

    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    $this->name = $row['name'];
    $this->brief_desc = $row['brief_desc'];
    $this->scheduled_date = $row['scheduled_date'];
    $this->header_image = $row['header_image'];
  }

  public function addUser() {
    $query = "INSERT INTO tbl_users (fullname, emailaddress) values ('". $this->fullname ."', '". $this->email ."')";

    $stmt = $this->conn->prepare($query);
    if($stmt->execute()) {
      return true;
    }

    printf("Error: %s.\n", $stmt->error);

    return false;
  }
}

?>