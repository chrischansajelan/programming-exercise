import React from 'react'

const Home = () => {

  return (
    <div className="row">
      <div className="col s12 m4 offset-m4">
        <div className="card">
          <div className="card-content">
            <h5>Join a webinar by entering the Webinar Name (e.g. /webinarname) in the URL</h5>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Home