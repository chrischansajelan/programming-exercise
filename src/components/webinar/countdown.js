import React, { useEffect, useState, useRef } from 'react'
import Cookies from 'universal-cookie'

const CountDown = (props) => {
  const [timerDays, setTimerDays] = useState('00')
  const [timerHours, setTimerHours] = useState('00')
  const [timerMinutes, setTimerMinutes] = useState('00')
  const [timerSeconds, setTimerSeconds] = useState('00')
  const [joinEnabled, setJoinEnabled] = useState(true)
  const [webinar, setWebinar] = useState({})
  const [isLoaded, setIsLoaded] = useState(false)
  const webinarName = props.match.params.webinarname
  const cookies = new Cookies()
  let interval = useRef()
  let distance

  useEffect(() => {
    getWebinar()

    return () => {
      clearInterval(interval.current)
    }
  }, [])

  const getWebinar = async () => {
    await fetch(`http://localhost/programming_exercise_backend/api/webinars/getsingle.php?webinarid=${webinarName}`)
      .then(response => response.json())
      .then(json => {
        setWebinar(json)
        startTimer(json)

        if(Object.values(json).length > 0) {
          setIsLoaded(true)
        }
      })
  }

  const startTimer = (json) => {
    const countdownDate = new Date(json.scheduled_date ? json.scheduled_date : '2020-09-15 22:14:00').getTime()

    interval = setInterval(() => {
      const now = new Date().getTime()
      distance = countdownDate - now
      

      const days = Math.floor(distance / (1000 * 60 * 60 *24))
      const hours = Math.floor((distance % (1000 * 60 * 60 *24) / (1000 * 60 * 60)))
      const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
      const seconds = Math.floor((distance % (1000 * 60)) / 1000)

      if(distance <= 0) {
        // stop timer
        setJoinEnabled(false)
        clearInterval(interval.current)
      } else {
        // update timer

        setTimerDays(days)
        setTimerHours(hours)
        setTimerMinutes(minutes)
        setTimerSeconds(seconds)
      }
    }, 1000)
  }

  const handleJoin = () => {
    cookies.set('current_webinar', webinarName, { path: '/' })
    window.location.href = "/" + webinarName
  }

  const handleNotFound = () => {
    window.location.href = '/notfound'
  }

  if(Object.values(webinar).length > 0 && webinar.name == null) {
    handleNotFound() 
  }

  if(isLoaded) {
    return (
      <div>
        <center>
          <div className="card-image" style={{ padding: 10 }}>
            <img src={webinar.header_image ? `data:image/jpeg;base64,${webinar.header_image}` : null} alt="webinar" />
          </div>
        </center>
        <div className="center-align">
          <h5>The webinar/meeting will start soon</h5>
        </div>
        <section className = "timer-container">
          <section className = "timer">
            <div>
              <section>
                <p>{timerDays}</p>
                <p><small>Days</small></p>
              </section>
              <span>:</span>
              <section>
                <p>{timerHours}</p>
                <p><small>Hours</small></p>
              </section>
              <span>:</span>
              <section>
                <p>{timerMinutes}</p>
                <p><small>Minutes</small></p>
              </section>
              <span>:</span>
              <section>
                <p>{timerSeconds}</p>
                <p><small>Seconds</small></p>
              </section>
            </div>
          </section>
        </section>
        <div className="center-align brief-desc">
          <p>{webinar.brief_desc ? webinar.brief_desc : "Brief description here"}</p>
        </div>
        <center>
          <button onClick={handleJoin} disabled={joinEnabled} className="waves-effect waves-light btn blue btn-large">Join Now</button>
        </center>
      </div>
    )
  }

  return (
    <center>
      <div className="preloader-wrapper big active">
        <div className="spinner-layer spinner-blue-only">
          <div className="circle-clipper left">
            <div className="circle"></div>
          </div><div className="gap-patch">
            <div className="circle"></div>
          </div><div className="circle-clipper right">
            <div className="circle"></div>
          </div>
        </div>
      </div>
    </center>
  )
}

export default CountDown