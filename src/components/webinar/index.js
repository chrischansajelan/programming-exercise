import React from 'react'

const WebinarIndex = () => {
  
  return (
    <div className="row" style={{ padding: 70 }}>
      <div className="col s12 white" style={{ height: 800, border: "1px solid #000" }}>
        <div className="col s9 white" style={{ height: "100%" }}>
          <div className="col s3 offset-s9 white center-align" style={{ height: "20%", border: "1px solid #000" }}>
            <h5>Cam Feed</h5>
          </div>
          <div className="col s12 white center-align" style={{ height: "70%", border: "1px solid #000" }}>
            <h1>Presentation</h1>
          </div>
          <div className="col s12" style={{ height: "10%" }}>
            <div className="row" style={{ padding: 5 }}>
              <div className="col s4 right-align">
                <a className="btn-floating btn-large waves-effect waves-light">
                  <i className="material-icons">mic</i>
                </a>
              </div>
              <div className="col s4 center-align">
                <a className="btn-floating btn-large waves-effect waves-light">
                  <i className="material-icons">camera_alt</i>
                </a>
              </div>
              <div className="col s4 left-align">
                <a className="btn-floating btn-large waves-effect waves-light">
                  <i className="material-icons red">fiber_manual_record</i>
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="col s3 white" style={{ height: "100%" }}>
          <div className="col s12 white center-align" style={{ height: "35%", border: "1px solid #000" }}>
            <h5>Participants</h5>
          </div>
          <div className="col s12 white center-align" style={{ height: "30%", border: "1px solid #000" }}>
            <h5>Chat</h5>
          </div>
          <div className="col s12 white center-align" style={{ height: "35%", border: "1px solid #000" }}>
            <h5>QnA/Polling</h5>
          </div>  
        </div>
      </div>
    </div>
  )
}

export default WebinarIndex