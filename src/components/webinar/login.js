import React, { useState, useEffect } from 'react'
import Cookies from 'universal-cookie'


const formatDate = (date) => {
  const options = { year: "numeric", month: "long", day: "numeric" }
  const dateUpdated = new Date(date)

  return dateUpdated.toLocaleDateString("en-US", options)
}

const formatTime = (date) =>{
  const d = new Date(date)
  const n = d.toLocaleTimeString()

  return n
}

const Login = (props) => {
  const [data, setData] = useState({})
  const [webinar, setWebinar] = useState({})
  const cookies = new Cookies()
  const webinarName = props.match.params.webinarname

  useEffect(() => {
    getWebinar();
  }, [])

  const getWebinar = async () => {
    await fetch(`http://localhost/programming_exercise_backend/api/webinars/getsingle.php?webinarid=${webinarName}`)
      .then(response => response.json())
      .then(json => setWebinar(json))
  }

  const handleChange = (event) => {
    const key = event.target.name;
    const value = event.target.value;

    setData(prevState => ({
      ...prevState,
      [key]: value
    }));
  }

  const handleSubmit = async (event) => {
    event.preventDefault()

    await fetch('http://localhost/programming_exercise_backend/api/users/add.php', {
      method: 'post',
      mode: 'no-cors',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        'fullname': data.fullname,
        'email': data.email
      })  
    })

    cookies.set('webinar_fullname', data.fullname, { path: '/' })
    cookies.set('webinar_email', data.email, { path: '/' })
    
    window.location.href = "/"+webinarName
  }

  const handleNotFound = () => {
    window.location.href = '/notfound'
  }

  if(Object.values(webinar).length > 0 && webinar.name == null) {
    handleNotFound() 
  }

  return (
    <form onSubmit={handleSubmit}>
      <div className="row">
        <div className="col s12 m4 offset-m4">
          <div className="card">
            <center>
              <div className="card-image" style={{ padding: 10 }}>
                <img src={webinar.header_image ? `data:image/jpeg;base64,${webinar.header_image}` : null} alt="webinar" />
              </div>
            </center>
            <div className="card-content center-align">
              <h3>{webinar.scheduled_date ? formatDate(webinar.scheduled_date) : ""}</h3>
              <h3>{webinar.scheduled_date ? formatTime(webinar.scheduled_date) : ""}</h3>
            </div>

            <div className="card-content">
              <h6 className="center-align">Please Login to Join</h6>
              <div className="form-field">
                <label>Full Name</label>
                <input type="text" name="fullname" onChange={handleChange} required/>
              </div><br/>

              <div className="form-field">
                <label>Email Address</label>
                <input type="email" name="email" onChange={handleChange} required/>
              </div><br/>

              <div className="form-field">
                <button type="submit" className="waves-effect waves-light btn blue" style={{ width: "100%" }}>Submit</button>
              </div><br/>
            </div>
          </div>
        </div>
      </div>
    </form>
  )
}

export default Login