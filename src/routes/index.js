import React from 'react'
import Cookies from 'universal-cookie'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Home from '../components/home'
import WebinarIndex from '../components/webinar/'
import Login from '../components/webinar/login'
import Countdown from '../components/webinar/countdown'
import NotFound from '../components/webinar/notfound'

const Routes = () => {
  const cookies = new Cookies()

  return (
    <BrowserRouter>
      <div className='App'>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/notfound' component={NotFound} />
          {cookies.get('webinar_email') ? 
            cookies.get('current_webinar') ? <Route path='/:webinarname' component={WebinarIndex} /> :
            <Route path='/:webinarname' component={Countdown} /> :
            <Route path='/:webinarname' component={Login} />
          }
          <Route path='/webinar/:webinarname' component={WebinarIndex} />
        </Switch>
      </div>  
    </BrowserRouter>
  );
}

export default Routes
